﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;

namespace WADOURI.Models
{
    /// <summary>
    /// Class containing all the parameters defined in the WADO-URI standard.
    /// </summary>
    public class RequestValues : System.Collections.CollectionBase
    {

        public string requestType { get; set; }
        public string studyUID { get; set; }
        public string seriesUID { get; set; }
        public string objectUID { get; set; }
        public string contentType { get; set; }

        public string transferSyntax { get; set; }

        //Other Parameters as defined in the DICOM WADO-URI standard,
        // for future implementations

        /*

        public string charset;
        public bool anonymize;
        public bool anotation;
        public int rows;
        public int columns;
        public string region;
        public int windowCenter;
        public int windowWidth;
        public int frameNumber;
        public int imageQuality;
        public int presentationUID;
        public int presentationSeriesUID;
        
        public string overlays;

        */
        
    }
}