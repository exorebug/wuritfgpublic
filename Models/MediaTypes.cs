﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WADOURI.Models
{
    /// <summary>
    /// Contains dictionary definitions for the supported request and content types.
    /// </summary>
    public static class MediaTypes
    {
        public static string defaultDicomTransferSyntax = "1.2.840.10008.1.2.1";

        public static Dictionary<string, int> supportedRequestType = new Dictionary<string, int>()
        {
           {"WADO", 1},        
        };
        
        /// <summary>
        /// This dictionary contains a list of supported media content types by the MV PACS server
        /// as key/value pairs. The values are the target formats of the server object/image retrieval
        /// service.
        /// Target formats: {0:dicom, 1:dicom(compressed), 2:winmedia 3:mpeg, 4:icon, 5:mp4}
        /// </summary>
        public static Dictionary<string, int> supportedContentType = new Dictionary<string, int>()
        {         
           {"application/dicom", 0},
           {"video/mpeg", 3},
           {"application/mp4", 5}
        };

    }

}