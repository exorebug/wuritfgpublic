﻿using WADOURI.Models;

namespace WADOURI
{
    /// <summary> Contains logic for request validation. </summary>
    public class  RequestValidator
    {
        /// <summary> Validates requests depending on the existence of request/content types
        /// in the MediaTypes model. </summary>
        /// <param name="reqValues">RequestValues object with request values.</param>
        /// <returns>A Boolean value indicating whether the validation was successful.</returns>
        public static bool validate (RequestValues reqValues)
        {

            /* Check for values that cannot be null and must be compatible
                with the server capabilities */
            if (reqValues.requestType == null || 
                !MediaTypes.supportedRequestType.ContainsKey(reqValues.requestType))
            {
                return false;
            }

            if (reqValues.contentType == null || 
                !MediaTypes.supportedContentType.ContainsKey(reqValues.contentType))
            {
                return false;
            }                        

            return true;            
        }
    }
}