﻿using System.Collections.Specialized;
using WADOURI.Models;

namespace WADOURI
{
    /// <summary> Contains methods for parsing request parameters. </summary>
    public class RequestParser
    {
        /// <summary> Parses URL query parameters. </summary>
        /// <param name="queryParams"></param>
        /// <returns>A RequestValues object populated with the request values.</returns>
        public static RequestValues parse (NameValueCollection queryParams)
        {
            var request = new RequestValues();

            //Iterate parameters and populate request values.
            foreach (var item in queryParams.AllKeys)
            {
                if ( request.GetType().GetProperty(item) != null) {
                    request.GetType().GetProperty(item).SetValue(request, queryParams[item]);
                    //System.Diagnostics.Debug.WriteLine("DEBUG property: " + item + "  set_to: " + queryParams[item]);
                }
            }
            return request;
        }

    }
}