﻿using System.Net;
using System.Web;
using WADOURI.Models;

namespace WADOURI
{
    /// <summary> WADO-URI service for the MV PACS. </summary>
    public class WadoUriService
    {
        /// <summary>
        /// Implements the WADO-URI service logic.
        /// </summary>
        /// <param name="context"></param>
        public static void processRequest(HttpContext context)
        {
            //Parse WADO request parameters
            RequestValues reqValues = RequestParser.parse(context.Request.QueryString);

            //Validate request values
            bool validationResult = RequestValidator.validate(reqValues);
            //bool validationResult = true;

            //Response
            HttpStatusCode errorCode;

            if (validationResult)
            {
                //Recover file path
                string objectPath;
                bool objectFound = RequestObjectFinder.findObject(reqValues, out objectPath);

                if (objectFound)
                {
                    //Write positive HTTP response
                    ResponseWriter.generateResponse(HttpContext.Current, objectPath, reqValues);
                }
                else
                {
                    //Write negative HTTP response
                    errorCode = HttpStatusCode.NotFound;
                    ResponseWriter.generateResponse(HttpContext.Current, errorCode);
                }
            }
            else
            {
                //Write negative HTTP response
                errorCode = HttpStatusCode.NotAcceptable;
                ResponseWriter.generateResponse(HttpContext.Current, errorCode);
            }
        }
    }
}