﻿using System;
using System.Net;
using System.Web;
using WADOURI.Models;

namespace WADOURI
{
    /// <summary> Contains methods to generate the apropiate response to a request. </summary>
    public class ResponseWriter
    {
        public static void generateResponse (HttpContext context, string path, RequestValues reqValues)
        {
            //System.Diagnostics.Debug.WriteLine("DEBUG Browser data: " + context.Request.Browser.Browser);
            //System.Diagnostics.Debug.WriteLine("DEBUG Browser accept header: " + context.Request.Headers);           
            /*
            context.Response.ContentType = reqValues.contentType;
            context.Response.WriteFile(path);
            context.Response.End();
            */
            System.IO.Stream iStream = null;

            // Buffer to read 10K bytes in chunk:
            byte[] buffer = new Byte[10000];

            // Length of the file:
            int length;

            // Total bytes to read:
            long dataToRead;

            // Identify the file to download including its path.
            string filepath = path;

            // Identify the file name.
            string filename = System.IO.Path.GetFileName(filepath);

            try
            {
                // Open the file.
                iStream = new System.IO.FileStream(filepath, System.IO.FileMode.Open,
                            System.IO.FileAccess.Read, System.IO.FileShare.Read);

                // Total bytes to read:
                dataToRead = iStream.Length;

                context.Response.ContentType = reqValues.contentType;
                context.Response.AddHeader("Content-length", dataToRead.ToString());
                //context.Response.AddHeader("Content-Disposition", "attachment; filename=" + filename);

                // Read the bytes.
                while (dataToRead > 0)
                {
                    // Verify that the client is connected.
                    if (context.Response.IsClientConnected)
                    {
                        // Read the data in buffer.
                        length = iStream.Read(buffer, 0, 10000);

                        // Write the data to the current output stream.
                        context.Response.OutputStream.Write(buffer, 0, length);                        

                        // Flush the data to the HTML output.
                        context.Response.Flush();

                        buffer = new Byte[10000];
                        dataToRead = dataToRead - length;
                    }
                    else
                    {
                        //prevent infinite loop if user disconnects
                        dataToRead = -1;
                    }
                }
            }
            catch (Exception ex)
            {
                // Trap the error, if any.
                context.Response.Write("Error : " + ex.Message);
            }
            finally
            {
                if (iStream != null)
                {
                    //Close the file.
                    iStream.Close();
                }
                context.Response.Close();
            }

        }

        public static void generateResponse (HttpContext context, HttpStatusCode errorCode)
        {
            context.Response.ContentType = "text/html";
            context.Response.StatusCode = (int)errorCode;
            //context.Response.Write(errorCode);
            context.Response.End();                        
        }
    }
}