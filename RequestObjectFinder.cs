﻿using System;
using WADOURI.Models;
using Lib_ISDispatcher;
using System.Xml;

namespace WADOURI
{
    /// <summary>Contains methods to interact with the PACS server and retrieve an object </summary>
    public class RequestObjectFinder
    {
        /// <summary> Searches for a given object defined by reqValues and returns the 
        /// file system path for its location </summary>
        /// <param name="reqValues">RequestValues object with request values.</param>
        /// <param name="pathToFile">Outbound string with file system path to file.</param>
        /// <returns>A Boolean value indicating whether the search was successful.</returns>
        public static bool findObject (RequestValues reqValues, out string pathToFile)
        {
            IISDispatcher objectDispatcher = null;
            string unparsedXML;
            int mediaType;
            pathToFile = null;

            //create COM object for information retrieval
            try
            {
                objectDispatcher = (IISDispatcher)new Lib_ISDispatcher.ISDispatcher();
            }
            catch (Exception e)
            {
                throw new Exception("Object Dispatcher failed to initialice.", e);
            }

            XmlDocument xmlData = new XmlDocument();
            XmlNamespaceManager namespaces = new XmlNamespaceManager(xmlData.NameTable);
            namespaces.AddNamespace("mvi", "http://www.nte.es/MVI/1.0/XMLSchema");

            // Recover and correct media type value and send request to object dispatcher

            MediaTypes.supportedContentType.TryGetValue(reqValues.contentType, out mediaType);
            /*
            if (mediaType == 0 && reqValues.transferSyntax == null) {
                reqValues.transferSyntax = MediaTypes.defaultDicomTransferSyntax;
            } else if (mediaType == 0 && reqValues.transferSyntax != null && reqValues.transferSyntax != MediaTypes.defaultDicomTransferSyntax) {
                mediaType = 1;
            }*/

            objectDispatcher.getImage(reqValues.objectUID, mediaType, reqValues.transferSyntax, 0, out unparsedXML); //Target format {0:dicom, 1:dicom(compressed), 2:winmedia 3:mpeg, 4:icon, 5:mp4

            if (unparsedXML == null) {
                System.Diagnostics.Debug.WriteLine("DEBUG: object dispatcher failed to find object");
                return false;
            }

            xmlData.LoadXml(unparsedXML);

            pathToFile = xmlData.SelectSingleNode("//mvi:imageLocation", namespaces).InnerText;

            return true;
        }
    }
}