﻿using System;
using System.Web;

namespace WADOURI
{
    public partial class wado : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Call the WADO-URI service logic.
            WadoUriService.processRequest(HttpContext.Current);

        }
    }
}
 